﻿using System.Numerics;

namespace Darari.Serialization.Extensions;
using Abstractions;
using Entities;

public static class NetworkReaderExtensions
{
    public static byte ReadByte(this INetworkReader reader) => reader.ReadBlittable<byte>();
    public static byte? ReadByteNullable(this INetworkReader reader) => reader.ReadBlittableNullable<byte>();

    public static sbyte ReadSByte(this INetworkReader reader) => reader.ReadBlittable<sbyte>();
    public static sbyte? ReadSByteNullable(this INetworkReader reader) => reader.ReadBlittableNullable<sbyte>();
    
    public static char ReadChar(this INetworkReader reader) => (char)reader.ReadBlittable<ushort>();
    public static char? ReadCharNullable(this INetworkReader reader) => (char?)reader.ReadBlittableNullable<ushort>();
    
    public static bool ReadBool(this INetworkReader reader) => reader.ReadBlittable<byte>() != 0;

    public static bool? ReadBoolNullable(this INetworkReader reader)
    {
        byte? value = reader.ReadBlittableNullable<byte>();
        return value.HasValue ? (value.Value != 0) : default(bool?);
    }

    public static short ReadShort(this INetworkReader reader) => (short)reader.ReadUShort();
    public static short? ReadShortNullable(this INetworkReader reader) => reader.ReadBlittableNullable<short>();

    public static ushort ReadUShort(this INetworkReader reader) => reader.ReadBlittable<ushort>();
    public static ushort? ReadUShortNullable(this INetworkReader reader) => reader.ReadBlittableNullable<ushort>();

    public static int ReadInt(this INetworkReader reader) => reader.ReadBlittable<int>();
    public static int? ReadIntNullable(this INetworkReader reader) => reader.ReadBlittableNullable<int>();

    public static uint ReadUInt(this INetworkReader reader) => reader.ReadBlittable<uint>();
    public static uint? ReadUIntNullable(this INetworkReader reader) => reader.ReadBlittableNullable<uint>();

    public static long ReadLong(this INetworkReader reader) => reader.ReadBlittable<long>();
    public static long? ReadLongNullable(this INetworkReader reader) => reader.ReadBlittableNullable<long>();

    public static ulong ReadULong(this INetworkReader reader) => reader.ReadBlittable<ulong>();
    public static ulong? ReadULongNullable(this INetworkReader reader) => reader.ReadBlittableNullable<ulong>();

    public static float ReadFloat(this INetworkReader reader) => reader.ReadBlittable<float>();
    public static float? ReadFloatNullable(this INetworkReader reader) => reader.ReadBlittableNullable<float>();

    public static double ReadDouble(this INetworkReader reader) => reader.ReadBlittable<double>();
    public static double? ReadDoubleNullable(this INetworkReader reader) => reader.ReadBlittableNullable<double>();

    public static decimal ReadDecimal(this INetworkReader reader) => reader.ReadBlittable<decimal>();
    public static decimal? ReadDecimalNullable(this INetworkReader reader) => reader.ReadBlittableNullable<decimal>();

    /// <exception cref="T:System.ArgumentException">if an invalid utf8 string is sent</exception>
    public static string ReadString(this INetworkReader reader)
    {
        ushort size = reader.ReadUShort();
        
        if (size == 0)
            return null;

        ushort realSize = (ushort)(size - 1);
        
        if (realSize > NetworkWriter.MaxStringLength)
            throw new EndOfStreamException(
                $"NetworkReader.ReadString - Value too long: {realSize} bytes. Limit is: {NetworkWriter.MaxStringLength} bytes");

        ArraySegment<byte> data = reader.ReadBytesSegment(realSize);
        
        return reader.Encoding.GetString(data.Array, data.Offset, data.Count);
    }

    public static byte[] ReadBytes(this INetworkReader reader, int count)
    {
        if (count > NetworkReader.AllocationLimit)
        {
            throw new EndOfStreamException(
                $"NetworkReader attempted to allocate {count} bytes, which is larger than the allowed limit of {NetworkReader.AllocationLimit} bytes.");
        }

        byte[] bytes = new byte[count];
        reader.ReadBytes(bytes, count);
        return bytes;
    }
    
    public static byte[] ReadBytesAndSize(this INetworkReader reader)
    {
        uint count = reader.ReadUInt();
        return count == 0 ? null : reader.ReadBytes(checked((int)(count - 1u)));
    }
    
    /// <exception cref="T:OverflowException">if count is invalid</exception>
    public static ArraySegment<byte> ReadArraySegmentAndSize(this INetworkReader reader)
    {
        uint count = reader.ReadUInt();
        return count == 0 ? default : reader.ReadBytesSegment(checked((int)(count - 1u)));
    }

    public static Vector2 ReadVector2(this INetworkReader reader) => reader.ReadBlittable<Vector2>();
    public static Vector2? ReadVector2Nullable(this INetworkReader reader) => reader.ReadBlittableNullable<Vector2>();

    public static Vector3 ReadVector3(this INetworkReader reader) => reader.ReadBlittable<Vector3>();
    public static Vector3? ReadVector3Nullable(this INetworkReader reader) => reader.ReadBlittableNullable<Vector3>();

    public static Vector4 ReadVector4(this INetworkReader reader) => reader.ReadBlittable<Vector4>();
    public static Vector4? ReadVector4Nullable(this INetworkReader reader) => reader.ReadBlittableNullable<Vector4>();

    public static Matrix4x4 ReadMatrix4x4(this INetworkReader reader) => reader.ReadBlittable<Matrix4x4>();

    public static Matrix4x4? ReadMatrix4x4Nullable(this INetworkReader reader) =>
        reader.ReadBlittableNullable<Matrix4x4>();

    public static Guid ReadGuid(this INetworkReader reader)
    {
#if !UNITY_2021_3_OR_NEWER
        // Unity 2019 doesn't have Span yet
        return new Guid(reader.ReadBytes(16));
#else
            // ReadBlittable(Guid) isn't safe. see ReadBlittable comments.
            // Guid is Sequential, but we can't guarantee packing.
            if (reader.Remaining >= 16)
            {
                ReadOnlySpan<byte> span =
 new ReadOnlySpan<byte>(reader.buffer.Array, reader.buffer.Offset + reader.Position, 16);
                reader.Position += 16;
                return new Guid(span);
            }
            throw new EndOfStreamException($"ReadGuid out of range: {reader}");
#endif
    }

    public static Guid? ReadGuidNullable(this INetworkReader reader) =>
        reader.ReadBool() ? ReadGuid(reader) : default(Guid?);
    
    public static List<T> ReadList<T>(this INetworkReader reader)
    {
        int length = reader.ReadInt();

        // 'null' is encoded as '-1'
        if (length < 0) return null;
        
        if (length > NetworkReader.AllocationLimit)
        {
            throw new EndOfStreamException(
                $"NetworkReader attempted to allocate a List<{typeof(T)}> {length} elements, which is larger than the allowed limit of {NetworkReader.AllocationLimit}.");
        }

        List<T> result = new List<T>(length);
        for (int i = 0; i < length; i++)
        {
            result.Add(reader.Read<T>());
        }

        return result;
    }

    public static T[] ReadArray<T>(this INetworkReader reader)
    {
        int length = reader.ReadInt();
        
        if (length < 0) return null;
        
        if (length > NetworkReader.AllocationLimit)
        {
            throw new EndOfStreamException(
                $"NetworkReader attempted to allocate an Array<{typeof(T)}> with {length} elements, which is larger than the allowed limit of {NetworkReader.AllocationLimit}.");
        }

        T[] result = new T[length];
        for (int i = 0; i < length; i++)
        {
            result[i] = reader.Read<T>();
        }

        return result;
    }

    public static Uri? ReadUri(this INetworkReader reader)
    {
        string uriString = reader.ReadString();
        return (string.IsNullOrWhiteSpace(uriString) ? null : new Uri(uriString));
    }

    public static DateTime ReadDateTime(this INetworkReader reader) => DateTime.FromOADate(reader.ReadDouble());

    public static DateTime? ReadDateTimeNullable(this INetworkReader reader) =>
        reader.ReadBool() ? ReadDateTime(reader) : default(DateTime?);
}