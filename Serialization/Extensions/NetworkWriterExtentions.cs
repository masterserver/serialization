﻿using System.Numerics;

namespace Darari.Serialization.Extensions;
using Abstractions;
using Entities;

public static class NetworkWriterExtensions
    {
        public static void WriteByte(this INetworkWriter writer, byte value) => writer.WriteBlittable(value);
        public static void WriteByteNullable(this INetworkWriter writer, byte? value) => writer.WriteBlittableNullable(value);

        public static void WriteSByte(this INetworkWriter writer, sbyte value) => writer.WriteBlittable(value);
        public static void WriteSByteNullable(this INetworkWriter writer, sbyte? value) => writer.WriteBlittableNullable(value);

        // char is not blittable. convert to ushort.
        public static void WriteChar(this INetworkWriter writer, char value) => writer.WriteBlittable((ushort)value);
        public static void WriteCharNullable(this INetworkWriter writer, char? value) => writer.WriteBlittableNullable((ushort?)value);

        // bool is not blittable. convert to byte.
        public static void WriteBool(this INetworkWriter writer, bool value) => writer.WriteBlittable((byte)(value ? 1 : 0));
        public static void WriteBoolNullable(this INetworkWriter writer, bool? value) => writer.WriteBlittableNullable(value.HasValue ? ((byte)(value.Value ? 1 : 0)) : new byte?());

        public static void WriteShort(this INetworkWriter writer, short value) => writer.WriteBlittable(value);
        public static void WriteShortNullable(this INetworkWriter writer, short? value) => writer.WriteBlittableNullable(value);

        public static void WriteUShort(this INetworkWriter writer, ushort value) => writer.WriteBlittable(value);
        public static void WriteUShortNullable(this INetworkWriter writer, ushort? value) => writer.WriteBlittableNullable(value);

        public static void WriteInt(this INetworkWriter writer, int value) => writer.WriteBlittable(value);
        public static void WriteIntNullable(this INetworkWriter writer, int? value) => writer.WriteBlittableNullable(value);

        public static void WriteUInt(this INetworkWriter writer, uint value) => writer.WriteBlittable(value);
        public static void WriteUIntNullable(this INetworkWriter writer, uint? value) => writer.WriteBlittableNullable(value);

        public static void WriteLong(this INetworkWriter writer, long value)  => writer.WriteBlittable(value);
        public static void WriteLongNullable(this INetworkWriter writer, long? value) => writer.WriteBlittableNullable(value);

        public static void WriteULong(this INetworkWriter writer, ulong value) => writer.WriteBlittable(value);
        public static void WriteULongNullable(this INetworkWriter writer, ulong? value) => writer.WriteBlittableNullable(value);

        public static void WriteFloat(this INetworkWriter writer, float value) => writer.WriteBlittable(value);
        public static void WriteFloatNullable(this INetworkWriter writer, float? value) => writer.WriteBlittableNullable(value);

        public static void WriteDouble(this INetworkWriter writer, double value) => writer.WriteBlittable(value);
        public static void WriteDoubleNullable(this INetworkWriter writer, double? value) => writer.WriteBlittableNullable(value);

        public static void WriteDecimal(this INetworkWriter writer, decimal value) => writer.WriteBlittable(value);
        public static void WriteDecimalNullable(this INetworkWriter writer, decimal? value) => writer.WriteBlittableNullable(value);

        public static void WriteString(this INetworkWriter writer, string value)
        {
            // write 0 for null support, increment real size by 1
            // (note: original HLAPI would write "" for null strings, but if a
            //        string is null on the server then it should also be null
            //        on the client)
            if (value == null || writer is not IEnsureNetworkWriter ensureWriter)
            {
                writer.WriteUShort(0);
                return;
            }

            // WriteString copies into the buffer manually.
            // need to ensure capacity here first, manually.
            int maxSize = writer.Encoding.GetMaxByteCount(value.Length);
            ensureWriter.EnsureCapacity(ensureWriter.Position + 2 + maxSize); // 2 bytes position + N bytes encoding

            // encode it into the buffer first.
            // reserve 2 bytes for header after we know how much was written.
            int written = writer.Encoding.GetBytes(value, 0, value.Length, ensureWriter.Buffer, ensureWriter.Position + 2);

            // check if within max size, otherwise Reader can't read it.
            if (written > NetworkWriter.MaxStringLength)
                throw new IndexOutOfRangeException($"NetworkWriter.WriteString - Value too long: {written} bytes. Limit: {NetworkWriter.MaxStringLength} bytes");

            // .Position is unchanged, so fill in the size header now.
            // we already ensured that max size fits into ushort.max-1.
            writer.WriteUShort(checked((ushort)(written + 1))); // Position += 2

            // now update position by what was written above
            ensureWriter.Position += written;
        }

        // Weaver needs a write function with just one byte[] parameter
        // (we don't name it .Write(byte[]) because it's really a WriteBytesAndSize since we write size / null info too)
        public static void WriteBytesAndSize(this INetworkWriter writer, byte[] buffer)
        {
            // buffer might be null, so we can't use .Length in that case
            writer.WriteBytesAndSize(buffer, 0, buffer != null ? buffer.Length : 0);
        }

        // for byte arrays with dynamic size, where the reader doesn't know how many will come
        // (like an inventory with different items etc.)
        public static void WriteBytesAndSize(this INetworkWriter writer, byte[] buffer, int offset, int count)
        {
            // null is supported because [SyncVar]s might be structs with null byte[] arrays
            // write 0 for null array, increment normal size by 1 to save bandwidth
            // (using size=-1 for null would limit max size to 32kb instead of 64kb)
            if (buffer == null)
            {
                writer.WriteUInt(0u);
                return;
            }
            writer.WriteUInt(checked((uint)count) + 1u);
            writer.WriteBytes(buffer, offset, count);
        }

        // writes ArraySegment of byte (most common type) and size header
        public static void WriteArraySegmentAndSize(this INetworkWriter writer, ArraySegment<byte> segment)
        {
            writer.WriteBytesAndSize(segment.Array, segment.Offset, segment.Count);
        }

        // writes ArraySegment of any type, and size header
        public static void WriteArraySegment<T>(this INetworkWriter writer, ArraySegment<T> segment)
        {
            int length = segment.Count;
            writer.WriteInt(length);
            for (int i = 0; i < length; i++)
            {
                writer.Write(segment.Array[segment.Offset + i]);
            }
        }

        public static void WriteVector2(this INetworkWriter writer, Vector2 value) => writer.WriteBlittable(value);
        public static void WriteVector2Nullable(this INetworkWriter writer, Vector2? value) => writer.WriteBlittableNullable(value);

        public static void WriteVector3(this INetworkWriter writer, Vector3 value) => writer.WriteBlittable(value);
        public static void WriteVector3Nullable(this INetworkWriter writer, Vector3? value) => writer.WriteBlittableNullable(value);

        public static void WriteVector4(this INetworkWriter writer, Vector4 value) => writer.WriteBlittable(value);
        public static void WriteVector4Nullable(this INetworkWriter writer, Vector4? value) => writer.WriteBlittableNullable(value);

        public static void WriteMatrix4x4(this INetworkWriter writer, Matrix4x4 value) => writer.WriteBlittable(value);
        public static void WriteMatrix4x4Nullable(this INetworkWriter writer, Matrix4x4? value) => writer.WriteBlittableNullable(value);

        public static void WriteGuid(this INetworkWriter writer, Guid value)
        {
#if !UNITY_2021_3_OR_NEWER
            // Unity 2019 doesn't have Span yet
            byte[] data = value.ToByteArray();
            writer.WriteBytes(data, 0, data.Length);
#else
            // WriteBlittable(Guid) isn't safe. see WriteBlittable comments.
            // Guid is Sequential, but we can't guarantee packing.
            // TryWriteBytes is safe and allocation free.
            writer.EnsureCapacity(writer.Position + 16);
            value.TryWriteBytes(new Span<byte>(writer.buffer, writer.Position, 16));
            writer.Position += 16;
#endif
        }
        public static void WriteGuidNullable(this INetworkWriter writer, Guid? value)
        {
            writer.WriteBool(value.HasValue);
            if (value.HasValue)
                writer.WriteGuid(value.Value);
        }

        // while SyncList<T> is recommended for NetworkBehaviours,
        // structs may have .List<T> members which weaver needs to be able to
        // fully serialize for NetworkMessages etc.
        // note that Weaver/Writers/GenerateWriter() handles this manually.
        public static void WriteList<T>(this INetworkWriter writer, List<T> list)
        {
            // 'null' is encoded as '-1'
            if (list is null)
            {
                writer.WriteInt(-1);
                return;
            }

            // check if within max size, otherwise Reader can't read it.
            if (list.Count > NetworkReader.AllocationLimit)
                throw new IndexOutOfRangeException($"NetworkWriter.WriteList - List<{typeof(T)}> too big: {list.Count} elements. Limit: {NetworkReader.AllocationLimit}");

            writer.WriteInt(list.Count);
            for (int i = 0; i < list.Count; i++)
                writer.Write(list[i]);
        }

        // while SyncSet<T> is recommended for NetworkBehaviours,
        // structs may have .Set<T> members which weaver needs to be able to
        // fully serialize for NetworkMessages etc.
        // note that Weaver/Writers/GenerateWriter() handles this manually.
        // TODO writer not found. need to adjust weaver first. see tests.
        /*
        public static void WriteHashSet<T>(this NetworkWriter writer, HashSet<T> hashSet)
        {
            if (hashSet is null)
            {
                writer.WriteInt(-1);
                return;
            }
            writer.WriteInt(hashSet.Count);
            foreach (T item in hashSet)
                writer.Write(item);
        }
        */

        public static void WriteArray<T>(this INetworkWriter writer, T[] array)
        {
            // 'null' is encoded as '-1'
            if (array is null)
            {
                writer.WriteInt(-1);
                return;
            }

            // check if within max size, otherwise Reader can't read it.
            if (array.Length > NetworkReader.AllocationLimit)
                throw new IndexOutOfRangeException($"NetworkWriter.WriteArray - Array<{typeof(T)}> too big: {array.Length} elements. Limit: {NetworkReader.AllocationLimit}");

            writer.WriteInt(array.Length);
            for (int i = 0; i < array.Length; i++)
                writer.Write(array[i]);
        }

        public static void WriteUri(this INetworkWriter writer, Uri uri)
        {
            writer.WriteString(uri?.ToString());
        }

        public static void WriteDateTime(this INetworkWriter writer, DateTime dateTime)
        {
            writer.WriteDouble(dateTime.ToOADate());
        }

        public static void WriteDateTimeNullable(this INetworkWriter writer, DateTime? dateTime)
        {
            writer.WriteBool(dateTime.HasValue);
            if (dateTime.HasValue)
                writer.WriteDouble(dateTime.Value.ToOADate());
        }
    }