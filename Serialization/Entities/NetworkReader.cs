﻿using System.Runtime.CompilerServices;
using System.Text;

namespace Darari.Serialization.Entities;
using Abstractions;

internal class NetworkReader: INetworkReader
{
    internal ArraySegment<byte> buffer;
    
    public int Position;
    
    public int Remaining => buffer.Count - Position;
    
    public int Capacity => buffer.Count;
    
    internal readonly UTF8Encoding encoding = new UTF8Encoding(false, true);

    public UTF8Encoding Encoding => encoding;
    
    public const int AllocationLimit = 1024 * 1024 * 16; // 16 MB * sizeof(T)

    public NetworkReader(ArraySegment<byte> segment)
    {
        buffer = segment;
    }

#if !UNITY_2021_3_OR_NEWER
    public NetworkReader(byte[] bytes)
    {
        buffer = new ArraySegment<byte>(bytes, 0, bytes.Length);
    }
#endif
    
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public void SetBuffer(ArraySegment<byte> segment)
    {
        buffer = segment;
        Position = 0;
    }

#if !UNITY_2021_3_OR_NEWER
    public void SetBuffer(byte[] bytes)
    {
        buffer = new ArraySegment<byte>(bytes, 0, bytes.Length);
        Position = 0;
    }
#endif
    
    public unsafe T ReadBlittable<T>()
        where T : unmanaged
    {
        // check if blittable for safety
#if UNITY_EDITOR
            if (!UnsafeUtility.IsBlittable(typeof(T)))
            {
                throw new ArgumentException($"{typeof(T)} is not blittable!");
            }
#endif
        
        int size = sizeof(T);

        // ensure remaining
        if (Remaining < size)
        {
            throw new EndOfStreamException($"ReadBlittable<{typeof(T)}> not enough data in buffer to read {size} bytes: {ToString()}");
        }

        // read blittable
        T value;
        fixed (byte* ptr = &buffer.Array[buffer.Offset + Position])
        {
#if UNITY_ANDROID
                // on some android systems, reading *(T*)ptr throws a NRE if
                // the ptr isn't aligned (i.e. if Position is 1,2,3,5, etc.).
                // here we have to use memcpy.
                //
                // => we can't get a pointer of a struct in C# without
                //    marshalling allocations
                // => instead, we stack allocate an array of type T and use that
                // => stackalloc avoids GC and is very fast. it only works for
                //    value types, but all blittable types are anyway.
                //
                // this way, we can still support blittable reads on android.
                // see also: https://github.com/vis2k/Mirror/issues/3044
                // (solution discovered by AIIO, FakeByte, mischa)
                T* valueBuffer = stackalloc T[1];
                UnsafeUtility.MemCpy(valueBuffer, ptr, size);
                value = valueBuffer[0];
#else
            value = *(T*)ptr;
#endif
        }
        Position += size;
        return value;
    }
    
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public T? ReadBlittableNullable<T>()
        where T : unmanaged =>
        ReadByte() != 0 ? ReadBlittable<T>() : default(T?);

    public byte ReadByte() => ReadBlittable<byte>();
    
    public byte[] ReadBytes(byte[] bytes, int count)
    {
        if (count < 0) throw new ArgumentOutOfRangeException("ReadBytes requires count >= 0");
        
        if (count > bytes.Length)
        {
            throw new EndOfStreamException($"ReadBytes can't read {count} + bytes because the passed byte[] only has length {bytes.Length}");
        }
        
        if (Remaining < count)
        {
            throw new EndOfStreamException($"ReadBytesSegment can't read {count} bytes because it would read past the end of the stream. {ToString()}");
        }

        Array.Copy(buffer.Array, buffer.Offset + Position, bytes, 0, count);
        Position += count;
        return bytes;
    }
    
    public ArraySegment<byte> ReadBytesSegment(int count)
    {
        if (count < 0) throw new ArgumentOutOfRangeException("ReadBytesSegment requires count >= 0");
        
        if (Remaining < count)
        {
            throw new EndOfStreamException($"ReadBytesSegment can't read {count} bytes because it would read past the end of the stream. {ToString()}");
        }
        
        ArraySegment<byte> result = new ArraySegment<byte>(buffer.Array, buffer.Offset + Position, count);
        Position += count;
        return result;
    }
    
    public T Read<T>()
    {
        Func<NetworkReader, T> readerDelegate = Reader<T>.read;
        if (readerDelegate == null)
        {
            var lastBackground = Console.BackgroundColor;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine($"<color=red>No reader found for {typeof(T)}. Use a type supported by Mirror or define a custom reader extension for {typeof(T)}.</color>");
            Console.BackgroundColor = lastBackground;
            return default;
        }
        return readerDelegate(this);
    }
    
    public override string ToString()
    {
        var arraySegment = buffer;
        var hexString = BitConverter.ToString(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
        return $"[{hexString} @ {Position}/{Capacity}]";
    }
}

public static class Reader<T>
{
    public static Func<INetworkReader, T> read;
}