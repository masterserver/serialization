﻿namespace Darari.Serialization.Entities;

internal class NetworkWriterPooled : NetworkWriter, IDisposable
{
    public void Dispose() => NetworkWriterPool.Return(this);
}