﻿using System.Runtime.CompilerServices;
using Darari.Serialization.Abstractions;

namespace Darari.Serialization.Entities;
using Utils;

public static class NetworkReaderPool
{
    static readonly Pool<NetworkReaderPooled> Pool = new Pool<NetworkReaderPooled>(
        () => new NetworkReaderPooled(new byte[]{}),
        1000
    );
    
    public static INetworkReader Get(byte[] bytes)
    {
        // grab from pool & set buffer
        NetworkReaderPooled reader = Pool.Get();
        reader.SetBuffer(bytes);
        return reader;
    }

    public static INetworkReader Get(ArraySegment<byte> segment)
    {
        // grab from pool & set buffer
        NetworkReaderPooled reader = Pool.Get();
        reader.SetBuffer(segment);
        return reader;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Return(INetworkReader reader)
    {
        if (reader is NetworkReaderPooled pooled)
        {
            Pool.Return(pooled);
        }
    }
}