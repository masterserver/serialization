﻿using System.Runtime.CompilerServices;
using Darari.Serialization.Abstractions;

namespace Darari.Serialization.Entities;
using Utils;

public static class NetworkWriterPool
{
    static readonly Pool<NetworkWriterPooled> Pool = new Pool<NetworkWriterPooled>(
        () => new NetworkWriterPooled(),
        1000
    );
    
    public static INetworkWriter Get()
    {
        NetworkWriterPooled writer = Pool.Get();
        writer.Reset();
        return writer;
    }
    
    
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Return(INetworkWriter writer)
    {
        if (writer is NetworkWriterPooled pooled)
        {
            Pool.Return(pooled);
        }
    }
}