using System.Runtime.CompilerServices;
using System.Text;

namespace Darari.Serialization.Entities;
using Abstractions;

internal class NetworkWriter: INetworkWriter, IEnsureNetworkWriter
{
    public const ushort MaxStringLength = ushort.MaxValue - 1;
    
        public const int DefaultCapacity = 1500;
        internal byte[] buffer = new byte[DefaultCapacity];

        public byte[] Buffer => buffer;
        
        public int Position { get; set; }
        
        public int Capacity => buffer.Length;

        internal readonly UTF8Encoding encoding = new UTF8Encoding(false, true);

        public UTF8Encoding Encoding => encoding;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void Reset()
        {
            Position = 0;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void EnsureCapacity(int value)
        {
            if (buffer.Length < value)
            {
                int capacity = Math.Max(value, buffer.Length * 2);
                Array.Resize(ref buffer, capacity);
            }
        }

        public byte[] ToArray()
        {
            byte[] data = new byte[Position];
            Array.ConstrainedCopy(buffer, 0, data, 0, Position);
            return data;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public ArraySegment<byte> ToArraySegment() =>
            new ArraySegment<byte>(buffer, 0, Position);

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static implicit operator ArraySegment<byte>(NetworkWriter w) =>
            w.ToArraySegment();

        public unsafe void WriteBlittable<T>(T value)
            where T : unmanaged
        {
            // check if blittable for safety
#if UNITY_EDITOR
            if (!UnsafeUtility.IsBlittable(typeof(T)))
            {
                Debug.LogError($"{typeof(T)} is not blittable!");
                return;
            }
#endif
            int size = sizeof(T);

            EnsureCapacity(Position + size);
            
            fixed (byte* ptr = &buffer[Position])
            {
#if UNITY_ANDROID
                // on some android systems, assigning *(T*)ptr throws a NRE if
                // the ptr isn't aligned (i.e. if Position is 1,2,3,5, etc.).
                // here we have to use memcpy.
                //
                // => we can't get a pointer of a struct in C# without
                //    marshalling allocations
                // => instead, we stack allocate an array of type T and use that
                // => stackalloc avoids GC and is very fast. it only works for
                //    value types, but all blittable types are anyway.
                //
                // this way, we can still support blittable reads on android.
                // see also: https://github.com/vis2k/Mirror/issues/3044
                // (solution discovered by AIIO, FakeByte, mischa)
                T* valueBuffer = stackalloc T[1]{value};
                UnsafeUtility.MemCpy(ptr, valueBuffer, size);
#else
                *(T*)ptr = value;
#endif
            }
            Position += size;
        }
        
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteBlittableNullable<T>(T? value)
            where T : unmanaged
        {
            WriteByte((byte)(value.HasValue ? 0x01 : 0x00));
            
            if (value.HasValue)
                WriteBlittable(value.Value);
        }

        public void WriteByte(byte value) => WriteBlittable(value);
        
        public void WriteBytes(byte[] array, int offset, int count)
        {
            EnsureCapacity(Position + count);
            Array.ConstrainedCopy(array, offset, this.buffer, Position, count);
            Position += count;
        }
        
        public unsafe bool WriteBytes(byte* ptr, int offset, int size)
        {
            EnsureCapacity(Position + size);

            fixed (byte* destination = &buffer[Position])
            {
                #if UNITY
                UnsafeUtility.MemCpy(destination, ptr + offset, size);
                #else
                System.Buffer.MemoryCopy(ptr + offset, destination, size, Capacity);
                #endif
            }

            Position += size;
            return true;
        }
        
        public void Write<T>(T value)
        {
            Action<NetworkWriter, T> writeDelegate = Writer<T>.write;
            if (writeDelegate == null)
            {
                var lastBackground = Console.BackgroundColor;
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine($"<color=red>No writer found for {typeof(T)}. This happens either if you are missing a NetworkWriter extension for your custom type, or if weaving failed. Try to reimport a script to weave again.</color>");
                Console.BackgroundColor = lastBackground;
            }
            else
            {
                writeDelegate(this, value);
            }
        }
        
        public override string ToString()
        {
            var arraySegment = ToArraySegment();
            var hexString = BitConverter.ToString(arraySegment.Array, arraySegment.Offset, arraySegment.Count);
            return $"[{hexString} @ {Position}/{Capacity}]";
        }
        
        public static class Writer<T>
        {
            public static Action<NetworkWriter, T> write;
        }
}