﻿namespace Darari.Serialization.Abstractions;

internal interface IEnsureNetworkWriter
{
    byte[] Buffer { get; }
    
    int Position { get; set; }

    void EnsureCapacity(int value);
}