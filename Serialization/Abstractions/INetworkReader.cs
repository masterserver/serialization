using System.Text;

namespace Darari.Serialization.Abstractions;

public interface INetworkReader
{
    UTF8Encoding Encoding { get; }
    
    T ReadBlittable<T>() where T : unmanaged;

    T? ReadBlittableNullable<T>() where T : unmanaged;
    
    T Read<T>();

    byte ReadByte();

    byte[] ReadBytes(byte[] bytes, int count);

    ArraySegment<byte> ReadBytesSegment(int count);
}