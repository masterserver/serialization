namespace Darari.Serialization.Abstractions;

public interface ISerializable
{
    public void Serialize(INetworkWriter writer);

    public void Deserialize(INetworkReader reader);
}