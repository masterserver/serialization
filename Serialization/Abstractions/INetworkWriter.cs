using System.Text;

namespace Darari.Serialization.Abstractions;

public interface INetworkWriter
{
    UTF8Encoding Encoding { get; }
    
    void WriteBlittable<T>(T value)
        where T : unmanaged;

    void WriteBlittableNullable<T>(T? value) where T : unmanaged;
    
    void WriteByte(byte value);

    void WriteBytes(byte[] array, int offset, int count);

    void Write<T>(T value);


    ArraySegment<byte> ToArraySegment();
    
    byte[] ToArray();
}